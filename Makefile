build:
	docker build -t markuman/gitea-user-heatmap .

run:
	docker run --rm -d --name gitea-user-heatmap -p 8000:8000 markuman/gitea-user-heatmap

stop:
	docker stop gitea-user-heatmap

debug:
	docker run --rm -ti --name gitea-user-heatmap --network dev -p 8000:8000 markuman/gitea-user-heatmap

service:
	docker service create --name gitea-user-heatmap --network db --network public markuman/gitea-user-heatmap

install:
	pip3 install -t $$(pwd) mariasql hug

clean:
	rm -rf *
	git reset --hard