# gitea-user-heatmap

use `make` for build, run and deploy.

# gitea integration

in `templates/user/profile.tmpl`

```diff
diff --git a/templates/user/profile.tmpl b/templates/user/profile.tmpl
index 6cac300..de755f4 100644
--- a/templates/user/profile.tmpl
+++ b/templates/user/profile.tmpl
@@ -82,6 +82,7 @@
                                </div>
                        </div>
                        <div class="ui eleven wide column">
+                                <iframe src="https://commits.git.osuv.de/index.html?user={{.Owner.Name}}" width="100%" height="225" frameborder="0"></iframe>
                                <div class="ui secondary stackable pointing menu">
                                        <a class='{{if and (ne .TabName "activity") (ne .TabName "stars")}}active{{end}} item' href="{{.Owner.HomeLink}}">
                                                <i class="octicon octicon-repo"></i> {{.i18n.Tr "user.repositories"}}
```

# sql setup

only tested with MariaDB 10.2 yet.

1. create a read only user for this service like that

```sql
CREATE USER 'read_only_user'@'%' ;
UPDATE mysql.user SET Password=PASSWORD('some_password') WHERE User='read_only_user' AND Host='%' ;
GRANT Select ON gitea.`action` TO 'read_only_user'@'%' ;
GRANT Select ON gitea.`user` TO 'commit_stats'@'%' ;
FLUSH PRIVILEGES;
```