import hug
import json
import mariasql
import os
import redis
import requests

SQL = """
SELECT
	COUNT( user_id ) AS count,
	DATE_FORMAT(
		FROM_UNIXTIME(
			`action`.created_unix
		),
		'%Y-%m-%d'
	) AS `date`
FROM
	`action`
INNER JOIN `user` ON
	(
		`user`.id =`action`.user_id
	)
WHERE
	`user`.lower_name = '{USERNAME}'
GROUP BY
	DATE_FORMAT(
		FROM_UNIXTIME(
			`action`.created_unix
		),
		'%Y-%m-%d'
	)
ORDER BY
	`action`.created_unix;
"""

@hug.get('/stats', examples='user=m')
def stats_by_user(user: hug.types.text):
    db = mariasql.MariaSQL(
        host = os.environ.get('DB_HOST') or "127.0.0.1",
        port = int(os.environ.get('DB_PORT') or 3306),
        user = os.environ.get('DB_USER') or "root",
        password = os.environ.get('DB_PASSWORD') or "nomysql1",
        db = os.environ.get('DB_DATABASE') or "gitea")
    retval = db.query(SQL.format(USERNAME=user))
    db.db.close()
    return retval

@hug.get('/stackoverflow', examples='user=2779972')
def get_stackoverflow(user: hug.types.text):
	r = redis.Redis(
		host=os.environ.get('REDIS_HOST') or 'localhost', 
		port=6379, 
		db=0)
	cached_value = r.get('STACKOVERFLOW:{USER}'.format(USER=user))
	if cached_value is None:
		url = "https://api.stackexchange.com/2.2/users/{USER}?order=desc&sort=reputation&site=stackoverflow".format(USER=user)
		resp = requests.get(url=url)
		data = resp.json()
		r.set('STACKOVERFLOW:{USER}'.format(USER=user), json.dumps(data), ex=300)
		return data
	return json.loads(cached_value)

@hug.static('/')
def index():
    return('./static',)
