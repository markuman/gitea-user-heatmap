FROM alpine:3.8

RUN apk --update --no-cache upgrade
RUN apk --update --no-cache add python3 alpine-sdk python3-dev \
  libffi libffi-dev openssl-dev \
  libev libev-dev

RUN pip3 install hug mariasql bjoern requests redis 

COPY main.py /main.py
COPY run_prod.py /run_prod.py
COPY static/ /static/

EXPOSE 8000

CMD hug -f main.py
# uwsgi --plugins http,python3  --http :8000 --wsgi-file main.py --callable __hug_wsgi__
#CMD python3 run_prod.py

